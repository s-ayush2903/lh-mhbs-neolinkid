'use strict'

const express = require('express')

const http = require('http')
const bodyParser = require('body-parser')
const readline = require('readline')
const fs = require('fs')
const sdk = require('./src/index')
const Spinner = require('cli-spinner').Spinner
const QRCode = require('qrcode')
const cors = require('cors')

const key = fs.readFileSync('/etc/letsencrypt/live/iwsg2020.crc.nd.edu/privkey.pem');
const cert = fs.readFileSync('/etc/letsencrypt/live/iwsg2020.crc.nd.edu/fullchain.pem');

const https = require('https');

var path = require('path');
var globalForDid

const LISTENING_PORT = 4000
const USER_PORT = 3000
const CONFIG_PATH = 'verity-context.json'
const INSTITUTION_NAME = 'DCR'
const LOGO_URL = 'http://robohash.org/235'
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
})

const handlers = new sdk.Handlers()
let listener
let userListener
let context
let issuerDID
let issuerVerkey

var didLookupMap = {}

const GuardianCredDefId =  "PCNdazQi3PWKDJRaiCJmDg:3:CL:115122:latest"
const NewbornCredDefId =  "PCNdazQi3PWKDJRaiCJmDg:3:CL:115124:latest"

async function startVerity()
{
  await setup()

//  const forDID = await createConnection()

  // was always commented out -  await askQuestion(forDID)


//  await issueCredential(forDID, defId)

//  await requestProof(forDID)
}

async function publishCredentials()
{
  const guardianSchemaId = await writeGuardianLedgerSchema()
  const guardianCredDefId = await writeLedgerCredDef(guardianSchemaId, "Guardian", "latest")

  const newbornSchemaId = await writeNewbornLedgerSchema()
  const newbornCredDefId = await writeLedgerCredDef(newbornSchemaId, "BND", "latest")
  console.log ("******* GuardianCredDefId = ", guardianCredDefId)
  console.log ("******* NewbornCredDefId = ", newbornCredDefId)
}

async function connectWebAppAndClient (res)
{
  const relDID = await createRelationshipResponse(res)
  await createConnection(relDID)
}

//* ***********************
//       RELATIONSHIP
//* ***********************
async function createRelationshipResponse (res) {
  // Relationship protocol has two steps
  // 1. create relationship key
  // 2. create invitation

  // Step 1

  // Constructor for the Connecting API
  const relProvisioning = new sdk.protocols.v1_0.Relationship(null, null, 'DCR (Informant)')
  var spinner = new Spinner('Waiting to create relationship ... %s').setSpinnerDelay(450) // Console spinner

  // handler for the response to the request to start the Relationship protocol.
  var firstStep = new Promise((resolve) => {
    handlers.addHandler(relProvisioning.msgFamily, relProvisioning.msgFamilyVersion, async (msgName, message) => {
      switch (msgName) {
        case relProvisioning.msgNames.CREATED:
          spinner.stop()
          console.log("firstStep of relationshipProvisioning Complete")
          printMessage(msgName, message)
          var threadId = message['~thread'].thid
          var relDID = message.did

          resolve([relDID, threadId])
          break
        default:
          printMessage(msgName, message)
          nonHandle('Message Name is not handled - ' + msgName)
      }
    })
  })

  spinner.start()
  // starts the relationship protocol
  await relProvisioning.create(context)
  const relationshipKeys = await firstStep // wait for response from verity application
  const relDID = relationshipKeys[0]
  const threadId = relationshipKeys[1]

  // Step 2

  spinner = new Spinner('Waiting to create invitation ... %s').setSpinnerDelay(450) // Console spinner
  // handler for the accept message sent when relationship is accepted
  var secondStep = new Promise((resolve) => {
    handlers.addHandler(relProvisioning.msgFamily, relProvisioning.msgFamilyVersion, async (msgName, message) => {
      switch (msgName) {
        case relationship.msgNames.INVITATION:
          spinner.stop()
          printMessage(msgName, message)

          console.log("Invitation created, being returned to calling web app...")
          console.log("relDID is ", relDID)

          var inviteURL = message.inviteURL
          console.log(inviteURL)

          // As for your question on the inviteUrl, the Aries protocols is producing InviteUrl in base64encoded format as per AriesRFC.
          // You should decode the c_i parameter from the inviteUrl and then turn that into a QR code for scan.

          await QRCode.toFile('qrcode.png', inviteURL)

          //console.log()
          //console.log('QR code at: qrcode.png')

          var QRData = await QRCode.toDataURL(inviteURL)
          didLookupMap[relDID] = relDID
          res.json({connectionId: relDID, QRCode: QRData})

          resolve(null)
          break
        default:
          printMessage(msgName, message)
          nonHandle('Message Name is not handled - ' + msgName)
      }
    })
  })

  spinner.start()
  const relationship = new sdk.protocols.v1_0.Relationship(relDID, threadId)

  await relationship.connectionInvitation(context)
  await secondStep // wait for acceptance from Client App user

  console.log("DID after issuing connectionInvitation is ", relDID)

  return relDID // return owning DID for the connection
}

//* ***********************
//       CONNECTION
//* ***********************
async function createConnection (relDID) {
  // Connecting protocol has to steps
  // 1. Start the protocol and receive the invite
  // 2. Wait for the other participant to accept the invite

  // Step 1

  console.log("Creating connection...")

  // Constructor for the Connecting API
  const connecting = new sdk.protocols.v1_0.Connecting(null, uuidv4(), null, true)
  var spinner = new Spinner('Waiting to start connection ... %s').setSpinnerDelay(450) // Console spinner

  // handler for the response to the request to start the Connecting protocol.
  var firstStep = new Promise((resolve) => {
    handlers.addHandler(connecting.msgFamily, connecting.msgFamilyVersion, async (msgName, message) => {
      switch (msgName) {
        case connecting.msgNames.REQUEST_RECEIVED:
          console.log("createConnection: 1")
          spinner.stop()
          printMessage(msgName, message)
          resolve(null)
          break
        default:
          printMessage(msgName, message)
          nonHandle('Message Name is not handled - ' + msgName)
      }
    })
  })

  spinner.start()
  // starts the connecting protocol
  await firstStep // wait for response from verity application

  // Step 2

  spinner = new Spinner('Waiting to respond to connection ... %s').setSpinnerDelay(450) // Console spinner
  // handler for the accept message sent when connection is accepted
  var secondStep = new Promise((resolve) => {
    handlers.addHandler(connecting.msgFamily, connecting.msgFamilyVersion, async (msgName, message) => {
      switch (msgName) {
        case connecting.msgNames.RESPONSE_SENT:
          console.log("createConnection: 2")
          spinner.stop()
          printMessage(msgName, message)
          resolve(null)
          break
        default:
          printMessage(msgName, message)
          nonHandle('Message Name is not handled - ' + msgName)
      }
    })
  })

  spinner.start()
  await secondStep // wait for acceptance from connect.me user
}


async function getProof (res, forConnection) {
  console.log("Requesting proof on connectionId: ", forConnection)

  //const schemaId = await writeLedgerSchema()
  //const defId = await writeLedgerCredDef(schemaId)

  //const defId = "PCNdazQi3PWKDJRaiCJmDg:3:CL:107273:latest"
  //await requestProof(didLookupMap[forConnection])
  await requestProof(res, forConnection) //For now, lets go direct...

  //res.json({result: forConnection})
  //res.json({status: "OK"})
}

//* ***********************
//       CONNECTION RESPONSE
//* ***********************


//* ***********************
//        SCHEMA
//* ***********************
async function writeGuardianLedgerSchema () {
  // input parameters for schema
  const schemaName = 'Guardian ' + uuidv4().substring(0, 8)
  const schemaVersion = '0.1'

  // These attrs need to match the keys that we tell the Informant form to give us...
  // Taken from: https://docs.google.com/spreadsheets/d/1C2AHCMn9zpxqE186mi2qBAFRyA_wYgPKi0wP7-iPrUw/edit#gid=921033112
  /*
    {key: "firstName", label: "First Name", props: {required: true}},
    {key: "middleName", label: "Middle Name", props: {required: true}},
    {key: "fathersName", label: "Fathers Name", props: {required: true}},
    {key: "subCounty", label: "Sub-County",  props: {required: true}},
    {key: "birthDate", label: "Birthdate", type: "date", props: {required: true}},
    {key: "idNumber", label: "ID Number", type: "number", props: {required: true}}
  */
  
  const schemaAttrs = ['firstName', 'middleName', 'fathersName', 'subCounty', 'birthDate', 'idNumber']

  // constructor for the Write Schema protocol
  var schemaId = await writeSchema (schemaName, schemaVersion, schemaAttrs)
  return schemaId // returns ledger schema identifier
}

async function writeNewbornLedgerSchema () {
  // input parameters for schema
  const schemaName = 'Newborn ' + uuidv4().substring(0, 8)
  const schemaVersion = '0.1'
 
  const schemaAttrs = ['firstName', 'middleName', 'fathersName', 'dateOfBirth', 'sex', 'typeOfBirth', 'subCounty', 'motherFirstName', 'motherMiddleName', 'motherFathersName', 'issuedTo', 'idNumber', 'issueDate']

  // constructor for the Write Schema protocol
  var schemaId = await writeSchema (schemaName, schemaVersion, schemaAttrs)
  return schemaId // returns ledger schema identifier
}


async function writeSchema (schemaName, schemaVersion, schemaAttrs) {
  // constructor for the Write Schema protocol
  const schema = new sdk.protocols.WriteSchema(schemaName, schemaVersion, schemaAttrs)
  var spinner = new Spinner('Waiting to write schema to ledger ... %s').setSpinnerDelay(450) // Console spinner

  // handler for message received when schema is written
  var firstStep = new Promise((resolve) => {
    handlers.addHandler(schema.msgFamily, schema.msgFamilyVersion, async (msgName, message) => {
      switch (msgName) {
        case schema.msgNames.STATUS:
          spinner.stop()
          printMessage(msgName, message)

          resolve(message.schemaId)
          break
        default:
          printMessage(msgName, message)
          nonHandle('Message Name is not handled - ' + msgName)
      }
    })
  })

  spinner.start()
  // request schema be written to ledger
  await schema.write(context) // wait for operation to be complete
  return firstStep // returns ledger schema identifier
}

//* ***********************
//        CRED DEF
//* ***********************
async function writeLedgerCredDef (schemaId, credDefName, credDefTag) {
  
  // constructor for the Write Credential Definition protocol
  const def = new sdk.protocols.WriteCredentialDefinition(credDefName, schemaId, credDefTag)
  var spinner = new Spinner('Waiting to write cred def to ledger ... %s').setSpinnerDelay(450) // Console spinner

  // handler for message received when schema is written
  var firstStep = new Promise((resolve) => {
    handlers.addHandler(def.msgFamily, def.msgFamilyVersion, async (msgName, message) => {
      switch (msgName) {
        case def.msgNames.STATUS:
          spinner.stop()
          printMessage(msgName, message)

          resolve(message.credDefId)
          break
        default:
          printMessage(msgName, message)
          nonHandle('Message Name is not handled - ' + msgName)
      }
    })
  })

  spinner.start()
  // request the cred def be writen to ledger
  await def.write(context) // wait for operation to be complete and returns ledger cred def identifier
  return firstStep
}

//* ***********************
//    ISSUE CREDENTIAL
//* ***********************

async function handleCredentialInvitationRequest(res, relDID, payload)
{
  console.log("API call to issue a credential to: ", relDID)

  //We need to unpick the payload data to populate the credential...
  var credentialId = payload.credentialId
  var credentialBody = JSON.parse(payload.credentialBody)
  
  await issueCredentialToClient(relDID, credentialId, credentialBody)

  console.log("Credential Invitation handled")
  res.json({status: "OK"})
}

async function issueCredentialToClient (relDID, credentialId, credentialBody) {
  // input parameters for issue credential
 
  console.log("credentialId is ", credentialId)
  console.log("Data sent to populate the credential is: ", JSON.stringify(credentialBody))

  var defId;
  var credentialDisplayName;
  
  // We will switch on "credentialId"...

  //Guardian cred...
  if (credentialId==='0')
  {
    credentialDisplayName = 'Guardian Identity'
    defId = GuardianCredDefId
  }
  else
  {
    credentialDisplayName = 'Birth Notification'
    defId = NewbornCredDefId
  }

  // constructor for the Issue Credential protocol
  const issue = new sdk.protocols.v1_0.IssueCredential(relDID, null, defId, credentialBody, credentialDisplayName, 0)
  var spinner = new Spinner('Wait for Client App to accept the Credential Offer ... %s').setSpinnerDelay(450) // Console spinner

  // handler for 'ask_accept` message when the offer for credential is accepted
  var firstStep = new Promise((resolve) => {
    handlers.addHandler(issue.msgFamily, issue.msgFamilyVersion, async (msgName, message) => {
      switch (msgName) {
        case issue.msgNames.SENT:
          spinner.stop()
          printMessage(msgName, message)

          resolve(null)
          break
        default:
          printMessage(msgName, message)
          nonHandle('Message Name is not handled - ' + msgName)
      }
    })
  })

  spinner.start()
  // request that credential is offered
  await issue.offerCredential(context)
  await firstStep // wait for Client App user to accept offer

  var secondStep = new Promise((resolve) => {
    handlers.addHandler(issue.msgFamily, issue.msgFamilyVersion, async (msgName, message) => {
      switch (msgName) {
        case issue.msgNames.ACCEPT_REQUEST:
          spinner.stop()
          printMessage(msgName, message)

          resolve(null)
          break
        default:
          printMessage(msgName, message)
          nonHandle('Message Name is not handled - ' + msgName)
      }
    })
  })

  spinner.start()
  await secondStep

  // request that credential be issued
  // handler for 'ask_accept` message when the offer for credential is accepted
  var thirdStep = new Promise((resolve) => {
    handlers.addHandler(issue.msgFamily, issue.msgFamilyVersion, async (msgName, message) => {
      switch (msgName) {
        case issue.msgNames.SENT:
          spinner.stop()
          printMessage(msgName, message)

          resolve(null)
          break
        default:
          printMessage(msgName, message)
          nonHandle('Message Name is not handled - ' + msgName)
      }
    })
  })

  spinner.start()
  // request that credential is offered
  await issue.issueCredential(context)
  await thirdStep // wait for Client App user to accept offer
}




//* ***********************
//         PROOF
//* ***********************
async function requestProof (res, forDID) {
  // input parameters for request proof
  const proofName = 'Proof of Degree' + uuidv4().substring(0, 8)
  const proofAttrs = [
    {
      name: 'name',
      restrictions: [{ issuer_did: issuerDID }]
    },
    {
      name: 'degree',
      restrictions: [{ issuer_did: issuerDID }]
    }
  ]

  // constructor for the Present Proof protocol
  const proof = new sdk.protocols.PresentProof(forDID, null, proofName, proofAttrs)
  var spinner = new Spinner('Waiting for proof presentation from Client App ... %s').setSpinnerDelay(450) // Console spinner

  // handler for the result of the proof presentation
  var firstStep = new Promise((resolve) => {
    handlers.addHandler(proof.msgFamily, proof.msgFamilyVersion, async (msgName, message) => {
      switch (msgName) {
        case proof.msgNames.PROOF_RESULT:
          spinner.stop()
          printMessage(msgName, message)
          res.json(message)
          console.log("Gone back")

          resolve(null)
          break
        default:
          printMessage(msgName, message)
          nonHandle('Message Name is not handled - ' + msgName)
      }
    })
  })
  spinner.start()

  // request proof
  await proof.request(context)
  return firstStep // wait for Client App user to present the requested proof
}

//* ***********************
//         SETUP
//* ***********************
async function setup () {
  if (fs.existsSync(CONFIG_PATH)) {
    if (await readlineYesNo('Reuse Verity Context (in ' + CONFIG_PATH + ')', true)) {
      context = await loadContext(CONFIG_PATH)
    } else {
      context = await provisionAgent()
    }
  } else {
    context = await provisionAgent()
  }

  await updateWebhookEndpoint()

  await updateConfigs()

  await issuerIdentifier()
  if (issuerDID == null) {
    await setupIssuer()
  }

  printObject(context.getConfig(), '>>>', 'Context Used:')
  fs.writeFileSync(CONFIG_PATH, JSON.stringify(context.getConfig()))

  console.log('Ready - please access via Informant App (access http://162.13.161.79:3000)')
}

async function loadContext (contextFile) {
  return sdk.Context.createWithConfig(fs.readFileSync(CONFIG_PATH))
}

async function provisionAgent () {
  var verityUrl = await readlineInput('Verity Application Endpoint')
  verityUrl = verityUrl.trim()
  if (verityUrl === '') {
    verityUrl = 'http://localhost:9000'
  }

  console.log('Using Url: ' + verityUrl)

  // create initial Context
  var ctx = await sdk.Context.create('examplewallet1', 'examplewallet1', verityUrl, '')
  const provision = new sdk.protocols.Provision()
  // ask that an agent by provision (setup) and associated with created key pair
  return provision.provisionSdk(ctx)
}

async function updateWebhookEndpoint () {
  var webhookFromCtx = context.endpointUrl

  var webhook = await readlineInput(`Ngrok endpoint for port(${LISTENING_PORT})[${webhookFromCtx}]`)
  if (webhook === '') {
    webhook = webhookFromCtx
  }

  console.log('Using Webhook: ' + webhook)
  context.endpointUrl = webhook

  // request that verity application use specified webhook endpoint
  await new sdk.protocols.UpdateEndpoint().update(context)
}

async function updateConfigs () {
  const updateConfigs = new sdk.protocols.UpdateConfigs(INSTITUTION_NAME, LOGO_URL)
  await updateConfigs.update(context)
}

async function setupIssuer () {
  // constructor for the Issuer Setup protocol
  const issuerSetup = new sdk.protocols.IssuerSetup()
  var spinner = new Spinner('Waiting for setup to complete ... %s').setSpinnerDelay(450) // Console spinner

  // handler for created issuer identifier message
  var step = new Promise((resolve) => {
    handlers.addHandler(issuerSetup.msgFamily, issuerSetup.msgFamilyVersion, async (msgName, message) => {
      switch (msgName) {
        case issuerSetup.msgNames.PUBLIC_IDENTIFIER_CREATED:
          spinner.stop()
          printMessage(msgName, message)
          issuerDID = message.identifier.did
          issuerVerkey = message.identifier.verKey
          console.log('The issuer DID and Verkey must be on the ledger.')
          console.log(`Please add DID (${issuerDID}) and Verkey (${issuerVerkey}) to ledger.`)
          await readlineInput('Press ENTER when DID is on ledger')
          resolve(null)
          break
        default:
          printMessage(msgName, message)
          nonHandle('Message Name is not handled - ' + msgName)
      }
    })
  })

  spinner.start()
  // request that issuer identifier be created
  await issuerSetup.create(context)
  return step // wait for request to complete
}

async function issuerIdentifier () {
  // constructor for the Issuer Setup protocol
  const issuerSetup = new sdk.protocols.IssuerSetup()
  var spinner = new Spinner('Waiting for current issuer DID ... %s').setSpinnerDelay(450)

  // handler for current issuer identifier message
  var step = new Promise((resolve) => {
    handlers.addHandler(issuerSetup.msgFamily, issuerSetup.msgFamilyVersion, async (msgName, message) => {
      spinner.stop()
      switch (msgName) {
        case issuerSetup.msgNames.PUBLIC_IDENTIFIER:
          printMessage(msgName, message)
          issuerDID = message.did
          issuerVerkey = message.verKey
          break
      }
      resolve(null)
    })
  })

  spinner.start()
  // query the current identifier
  await issuerSetup.currentPublicIdentifier(context)
  return step // wait for response from verity application
}

//* ***********************
//         MAIN
//* ***********************
main()

async function main () {
  await start()
  await startVerity()
  
  //await issueCredentialInvitation("3iaJdq7p2JdxKFXNuMvJi7", 0, 0)
  //await publishCredentials()

  //await example()
  //await end()
}

async function start () {
  const app = express()
  app.use(bodyParser.text({
    type: function (_) {
      return 'text'
    }
  }))

  app.post('/', async (req, res) => {
    await handlers.handleMessage(context, Buffer.from(req.body, 'utf8'))
    res.send('Success')
  })

  listener = http.createServer(app).listen(LISTENING_PORT)
  console.log(`Listening on port ${LISTENING_PORT}`)

  const userApp = express()
  userApp.options('*', cors());

  userApp.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Cache-Control', 's-maxage=0')

    //res.set('Cache-Control', 'public, max-age=0');
    next();
  });

  userApp.use(bodyParser.json());
  userApp.use(bodyParser.urlencoded());

  // Start of API definition
  userApp.post('/DCR/v1/credential', function(req, res) {

    console.log("POST Data is", req.body);
    var connectionId = req.body.connectionId;
    const did = req.body.connectionId;

    if (did === undefined)
      res.send("No DID sent")
    else
      handleCredentialInvitationRequest(res, did, req.body)
  });

  // Start of API definition
  userApp.get('/DCR/v1/connectionInvitation', function(req, res) {
    // Returns JSON containing QR code...
    connectWebAppAndClient(res)


  });

  userApp.get('/DCR/v1/credentialSchema/', function(req, res) {
    // Returns JSON list of all the credentials that can be offered...
    
    var credentialList = [] //We'll populate this from a json file of the credentials we can offer
    credentialList.push({credentialId: 0, credentialName: "Guardian"}) 
    credentialList.push({credentialId: 1, credentialName: "Birth Notification"})

   // credentialList.push({credentialId: 1, credentialName: "New Boy"})
   // credentialList.push({credentialId: 2, credentialName: "New Girl"})

    res.json(credentialList)
  });

  userApp.get('/DCR/v1/credentialSchema/:id', function(req, res) {
    // Returns credential schema for the passed id
    
      var output = {}
      var formFields = []    
      
      // The keys need to match what is in the credential...
      switch (req.params.id)
      {
        case '0':
            output.title = "Guardian"
            formFields.push(
              {key: "firstName", label: "First Name", props: {required: true}},
              {key: "middleName", label: "Middle Name", props: {required: true}},
              {key: "fathersName", label: "Father's Name", props: {required: true}},
              {key: "subCounty", label: "Sub-County",  type: "dropdown", props: {required: true}, values:["Turbo", "Kesses", "Moiben", "Kapseret", "Soy", "Ainabkoi"]},
              {key: "birthDate", label: "Birthdate", type: "date", props: {required: true}},
              {key: "idNumber", label: "ID Number", type: "number", props: {required: true, minLength:7, minLength:8}, session: true}
            );
            break;
        case '1':
            //output = '{ title: "Birth Credential", fields: '
            output.title = "Birth Notification"
            formFields.push(
              {key:"firstName", label: "Child's First Name", props: {required: true}},
              {key:"middleName", label: "Child's Middle Name", props: {required: true}},
              {key:"fathersName", label: "Child's Fathers Name", props: {required: true}},
              {key:"dateOfBirth", label: "Child's Birthdate", type: "date", props: {required: true}},
              {key:"sex", label: "Child's Sex", type: "dropdown", props: {required: true}, values:["Male", "Female"]},
              {key:"typeOfBirth", label: "Type of Birth", type: "dropdown", props: {required: true}, values:["Single", "Twin", "Other"]},
              //{key:"natureOfBirth", label: "Nature of Birth", props: {required: true}, values:["Alive", "Dead"]},
              //{key:"placeOfBirth", label: "Place of Birth", props: {required: true}},
              {key:"subCounty", label: "Sub-County", type: "dropdown", props: {required: true}, values:["Turbo", "Kesses", "Moiben", "Kapseret", "Soy", "Ainabkoi"]},
              {key:"motherFirstName", label: "Mother's First Name", props: {required: true}},
              {key:"motherMiddleName", label: "Mother's Middle Name", props: {required: true}},
              {key:"motherFathersName", label: "Mother's Father's Name", props: {required: true}},
              {key:"issuedTo", label: "Issued to (Name)", props: {required: true}},
              {key:"idNumber", label: "ID Number", type: "number", props: {required: true}, session: true},
              {key:"issueDate", label: "Issue Date", type: "date", props: {required: true, value: "08/07/2020"}}
            );
            break;   
        case '2':
            output.title = "Birth Notification"
            formFields.push({fieldName: "motherName", displayName: "Mother Name", type: "string", range: "", required: "true"	})
            break;   
      }

      output.formFields = formFields

      res.json(output)
  });

  userApp.get('/policy/:id', function(req, res) {
    console.log("Request proof for id: ", req.params.id)
    getProof (res, req.params.id)
    
  });

    // This is only really used for testing, as QR code is sent in JSON
    userApp.get('/qrcode.png', function(req, res) {
      res.sendFile('/root/VeritySDK/sdk/nodejs-sdk/'+req.path);
    });

    // Only for testing...
    userApp.get('/credential', function(req, res) {
      
    const did = req.query.did

    if (did === undefined)
      res.send("No DID sent")
    else
      handleCredentialInvitationRequest(res, did, 0)

  });

  userListener = https.createServer({key: key, cert: cert }, userApp);
  userListener.listen(USER_PORT)

  //userListener = http.createServer(userApp).listen(USER_PORT)
  console.log(`Listening on port ${USER_PORT}`)
}

async function end () {
  listener.close()
  rl.close()
  process.exit(0)
}

//* ***********************
//         UTILS
//* ***********************

// Simple utility functions.

async function readlineInput (request) {
  console.log()

  return new Promise((resolve) => {
    rl.question(request + ': ', (response) => { resolve(response) })
  })
}

async function readlineYesNo (request, defaultYes) {
  var yesNo = defaultYes ? '[y]/n' : 'y/n'
  var modifiedRequest = request + '? ' + yesNo + ': '

  return new Promise((resolve) => {
    rl.question(modifiedRequest, (response) => {
      var normalized = response.trim().toLocaleLowerCase()
      if (defaultYes && normalized === '') {
        resolve(true)
      } else if (normalized === 'y') {
        resolve(true)
      } else if (normalized === 'n') {
        resolve(false)
      } else {
        console.error("Did not get a valid response -- '" + response + "' is not y or n")
        process.exit(-1)
      }
    })
  })
}

function printMessage (msgName, msg) {
  printObject(msg, '<<<', `Incoming Message -- ${msgName}`)
}

function printObject (obj, prefix, preamble) {
  console.log()
  console.log(prefix + '  ' + preamble)
  var lines = JSON.stringify(obj, null, 2).split('\n')
  lines.forEach(line => {
    console.log(prefix + '  ' + line)
  })
  console.log()
}

function nonHandle (msg) {
  console.error(msg)
  // We don't want to exit - we may need to do something else to cleanup... 
  //process.exit(-1)
}

function uuidv4 () {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
    var r = Math.random() * 16 | 0; var v = c === 'x' ? r : (r & 0x3 | 0x8)
    return v.toString(16)
  })
}

function sleep (ms) {
  return new Promise(resolve => setTimeout(resolve, ms))
}
