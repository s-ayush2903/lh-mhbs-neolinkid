package com.example.guardianapp;

/*
 old code for old verity UI.
 */
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;

import com.evernym.sdk.vcx.VcxException;
import com.evernym.sdk.vcx.connection.ConnectionApi;
import com.evernym.sdk.vcx.utils.UtilsApi;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import org.json.*;
//import org.json.JSONException;
//import org.json.JSONObject;

public class MainActivity extends BaseActivity {

    public static final String TAG = "MainActivity";

    private boolean sdkInited = false;
    //private ConnectMeVcxOld sdkApi;
    private String connectionQRcode = "";
    private String connectionSerializedResponse = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*//Init the sdkApi
        sdkApi = new ConnectMeVcxOld(this);
        //String w = sdkApi.init();
        sdkApi.init();*/

        //Log.d(TAG,"work"+w);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /*FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
    }

    public void scanQR(View v) {
        startActivityForResult(new Intent(MainActivity.this, QRScannerActivity.class), 1);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if(data != null) {
            if (requestCode == 1) {
                EditText editText = (EditText) findViewById(R.id.et_scanned_content);
                String message = data.getStringExtra("MESSAGE");
                editText.setText(message);
                connectionQRcode = message;
            }
        }
    }

    public void addConnectionOnClick(View v) {
        /*EditText editText   = (EditText)findViewById(R.id.et_scanned_content);
        String invitationDetails = editText.getText().toString();
        Log.d(TAG, "connection invitation is set to: " + invitationDetails);

        try {
            JSONObject json = new JSONObject(invitationDetails);
            sdkApi.createConnectionWithInvite(json.getString("id"), invitationDetails, new CompletableFuturePromise<>(connectionHandle -> {
                Log.e(TAG, "createConnectionWithInvite return code is: " + connectionHandle);
                if(connectionHandle != -1) {
                    sdkApi.vcxAcceptInvitation(connectionHandle, "{\"connection_type\":\"QR\",\"phone\":\"\"}", new CompletableFuturePromise<>(inviteDetails -> {
                        Log.e(TAG, "vcxAcceptInvitation return code is: " + inviteDetails);
                        if(invitationDetails != null) {
                            sdkApi.getSerializedConnection(connectionHandle, new CompletableFuturePromise<>(state -> {
                                Log.e(TAG, "getSerializedConnection returned state is: " + state);
                                connectionSerializedResponse = state;
                            }, (t) -> {
                                Log.e(TAG, "getSerializedConnection error is: ", t);
                                return null;
                            }));
                        }
                    }, (t) -> {
                        Log.e(TAG, "vcxAcceptInvitation error is: ", t);
                        return null;
                    }));
                }
            }, (t) -> {
                Log.e(TAG, "createConnectionWithInvite error is: ", t);
                return -1;
            }));
        } catch (JSONException e) {
            e.printStackTrace();
        }*/
    }

    public void acceptCredentialOffer(View v) {
        String messageStatus = "MS-103,MS-101,MS-102,MS-104,MS-105";
        try {
            UtilsApi.vcxGetMessages(messageStatus, null, null).exceptionally((t) -> {
                Log.e(TAG, "vcxGetMessages: ", t);
                return null;
            }).thenAccept(result -> {
                //Log.d(TAG, "result vcxGetMessages: " + result);
                //Log.d(TAG,"Length of result before:: "+result.length() +".");
                result = result.replace("\\","");
                //result = result.replace("@","");
                result = result.replace("\"{","{");
                result = result.replace("}\"","}");
                result = result.replace("\"[","[");
                result = result.replace("]\"","]");

               // Log.d(TAG,"Length of result after:: "+result.length() +".");
               // Log.d(TAG,"3000th character to last character of result:: "+result.substring(3000,result.length()));
                try {
                    JSONArray jsonArray = new JSONArray(result);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject explrObject = jsonArray.getJSONObject(i);
                        Log.d("JSON obj @"+i+"::", explrObject.toString()+"!");
                        Log.d("pairwiseDID",explrObject.getString("pairwiseDID"));
                        Log.d("pairwiseDID",
                                String.valueOf(explrObject.getJSONArray("msgs")));

                        JSONArray msgs = explrObject.getJSONArray("msgs");
                        for(int j = 0;j<msgs.length(); j++) {
                            JSONObject msgObject = msgs.getJSONObject(j);
                            Log.d("msgs @"+j+"::", msgObject.toString()+"!");
                            Log.d("msg attr statusCode",msgObject.getString("statusCode"));
                            Log.d("msg attr payload",msgObject.getString("payload")+"!");

                            if(j==2) {
                                Log.d("msgs",msgObject.get("decryptedPayload")+"!");
                                Log.d("msgs",msgObject.getString("decryptedPayload")+"!");
                                JSONObject decrypt = new JSONObject(msgObject.getString("decryptedPayload"));
                                Log.d("decrypt",decrypt.getString("@type"));
                                Log.d("decrypt",decrypt.getString("@msg"));
                            }
                        }
                    }
                }catch (JSONException err){ //4233
                    Log.d("Error", err.toString()/*.substring(err.toString().length()-1500,err.toString().length())*/);
                }
            });
        } catch (VcxException e) {
            e.printStackTrace();
        }
    }

    /*public void deserialize(){
        sdkApi.deserializeConnection(connectionSerializedResponse,);
    }*/

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
       /* switch (requestCode) {
            case VcxStaticData.REQUEST_WRITE_EXTERNAL_STORAGE: {
                if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    VcxStaticData.initLoggerFile(this);
                    Toast.makeText(this,
                            "File system access allowed",
                            Toast.LENGTH_SHORT).show();

                    // resolve the promise for file system access
                    VcxStaticData.resolveLoggerPromise(VcxStaticData.LOG_FILE_PATH);
                } else {
                    Toast.makeText(this,
                            "File system access NOT allowed",
                            Toast.LENGTH_SHORT).show();

                    // reject the promise for file system access
                    VcxStaticData.rejectLoggerPromise("ERR-104", "File system access NOT allowed");
                }
                return;
            }
        }
*/
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
