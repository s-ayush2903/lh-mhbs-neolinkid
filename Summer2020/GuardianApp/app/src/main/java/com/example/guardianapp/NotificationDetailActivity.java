package com.example.guardianapp;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import com.example.guardianapp.databinding.ActivityNotificationDetailBinding;
import com.example.guardianapp.db.entity.Credential;
import com.example.guardianapp.model.Attribute;
import com.example.guardianapp.ui.home.Child;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Objects;

import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

public class NotificationDetailActivity extends BaseActivity {

    private static String TAG = NotificationDetailActivity.class.getName();
    private Credential credential;
    private int position;
    private ActivityNotificationDetailBinding mNotificationDetailBinding;
    private CredentialOffersViewModel model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // implemented view binding
        mNotificationDetailBinding = ActivityNotificationDetailBinding.inflate(getLayoutInflater());
        setContentView(mNotificationDetailBinding.getRoot());

        // recycler view for list of attributes
        mNotificationDetailBinding.rvAttributes.setHasFixedSize(false);
        mNotificationDetailBinding.rvAttributes.setLayoutManager(new LinearLayoutManager(this));

        // back button to close the activity
        mNotificationDetailBinding.ibClose.setOnClickListener(v -> finish());

        // retrieving credential offer object
        credential = Objects.requireNonNull(getIntent().getExtras()).getParcelable(getString(R.string.credential_offer));

        SingletonClass singletonClass = SingletonClass.getInstance(getApplicationContext());
        if (singletonClass.isInDemoMode()) { // screen opened in DEMO mode
            position = getIntent().getExtras().getInt(getString(R.string.position));
        } else {
            model = new ViewModelProvider(this).get(CredentialOffersViewModel.class);
        }

        if(credential.getDateTimeString() != null){
            mNotificationDetailBinding.tvDatetime.setText(credential.getDateTimeString());
        } else {
            mNotificationDetailBinding.tvDatetime.setVisibility(View.GONE);
        }
        mNotificationDetailBinding.tvTitle.setText(credential.getName());
        mNotificationDetailBinding.tvOrganizationName.setText(credential.getOrganizationName());
        showAttributes();
        statusOfCredential();

        mNotificationDetailBinding.btnAccept.setOnClickListener(v -> {

            SingletonClass singleton = SingletonClass.getInstance(getApplicationContext());

            if(singleton.isInDemoMode()) {
                SingletonClass.getInstance(getApplicationContext()).acceptedCredentialOffer(
                        position, credential);

                if (credential.getType().equals("GUARDIAN_CREDENTIAL")) {
                    SingletonClass.getInstance(getApplicationContext()).setGuardianCredential(
                            credential);
                } else if (credential.getType().contains("BIRTH_NOTIFICATION_CREDENTIAL")) {
                    Child child = new Child(credential, new ArrayList<>());
                    SingletonClass.getInstance(getApplicationContext()).getChildren().add(
                            child);
                } else if (credential.getType().contains("GUARDIAN_LINK_CREDENTIAL")) {
                    SingletonClass.getInstance(
                            getApplicationContext()).getGuardianDemo().getCredentials().add(
                            credential);
                } else if (credential.getType().contains("VACCINE_CREDENTIAL")) {
                    if (credential.getType().contains("1")) {
                        SingletonClass.getInstance(getApplicationContext()).getChildren().get(
                                0).getCredentials().add(credential);
                    } else if (credential.getType().contains("2")) {
                        SingletonClass.getInstance(getApplicationContext()).getChildren().get(
                                1).getCredentials().add(credential);
                    } else if (credential.getType().contains("3")) {
                        SingletonClass.getInstance(getApplicationContext()).getChildren().get(
                                2).getCredentials().add(credential);
                    }
                }
                mNotificationDetailBinding.tvAccepting.setText(
                        String.format("Accepting %s Credential", credential.name));
                mNotificationDetailBinding.clAcceptCredential.setVisibility(View.VISIBLE);
                new Handler().postDelayed(() -> {
                    finish();
                }, 2000);

            } else {
                mNotificationDetailBinding.tvAccepting.setText(
                        String.format("Accepting %s Credential", credential.name));
                mNotificationDetailBinding.clAcceptCredential.setVisibility(View.VISIBLE);
                runOnUiThread(() -> model.acceptOffer(credential.id).observeOnce(this, credential -> {
                    this.credential = credential;
                    mNotificationDetailBinding.clAcceptCredential.setVisibility(View.GONE);
                    mNotificationDetailBinding.btnAccept.setVisibility(View.GONE);
                    statusOfCredential();
                    mNotificationDetailBinding.tvDatetime.setText(credential.getDateTimeString());
                    mNotificationDetailBinding.tvDatetime.setVisibility(View.VISIBLE);
                    //finish();
                }));

            }

        });

        mNotificationDetailBinding.btnReject.setOnClickListener(v -> {
            /*credential.setAccepted(false);
            credential.setNeedAction(false);
            SingletonClass.getInstance(
                    getApplicationContext()).setNoPendingCredentialOffers(
                    true);
            SingletonClass.getInstance(
                    getApplicationContext()).getCredentialList().set(position,
                    credential);
            finish();*/
        });

    }

    private void showAttributes() {
        Iterator<?> keys = credential.getJSONObject().keys();
        ArrayList<Attribute> attributes = new ArrayList<>();
        while (keys.hasNext()) { // parsing the attributes JSON object into list of Attribute
            // objects
            String key = (String) keys.next();
            try {
                Attribute attribute = new Attribute(key,
                        credential.getJSONObject().get(key).toString());
                attributes.add(attribute);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        AttributesAdapter attributesAdapter = new AttributesAdapter(attributes, this);
        mNotificationDetailBinding.rvAttributes.setAdapter(attributesAdapter);
    }

    private void statusOfCredential(){
        String message;
        if (credential.getNeedAction()) { // if the credential offer has not been accepted or rejected
            message = String.format("STATUS: %s", getString(R.string.pending));
        } else {
            if (credential.getAccepted()) { // if the credential offer is already accepted
                message = String.format("STATUS: %s", getString(R.string.accepted));
            } else { // if the credential offer is already rejected
                message = String.format("STATUS: %s", getString(R.string.rejected));
            }
            mNotificationDetailBinding.btnAccept.setVisibility(View.GONE);
            mNotificationDetailBinding.btnReject.setVisibility(View.GONE);
        }
        mNotificationDetailBinding.tvStatus.setText(message);
    }
}