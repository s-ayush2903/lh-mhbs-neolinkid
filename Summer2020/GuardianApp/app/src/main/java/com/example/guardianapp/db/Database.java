package com.example.guardianapp.db;

import android.content.Context;

import com.example.guardianapp.db.dao.ConnectionDao;
import com.example.guardianapp.db.dao.CredentialDao;
import com.example.guardianapp.db.entity.Connection;
import com.example.guardianapp.db.entity.Credential;

import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

@androidx.room.Database(entities = {Connection.class, Credential.class},
        version = 1)
@TypeConverters({ResponseConverter.class})
public abstract class Database extends RoomDatabase {
    private static final String DB_NAME = "db";
    private static Database instance = null;

    public abstract ConnectionDao connectionDao();

    public abstract CredentialDao credentialsDao();

    public static Database getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(), Database.class, DB_NAME).allowMainThreadQueries().build();
        }
        return instance;
    }
}
