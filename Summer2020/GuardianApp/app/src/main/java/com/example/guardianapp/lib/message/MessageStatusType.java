package com.example.guardianapp.lib.message;

public interface MessageStatusType {
    String CREATED = "MS-101";
    String SENT = "MS-102";
    String PENDING = "MS-103";
    String ACCEPTED = "MS-104";
    String REJECTED = "MS-105";
    String ANSWERED = "MS-106";
}
