/*
 not used as of now
 */

/*
package com.example.guardianapp.ui.notifications;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONException;
import org.json.JSONObject;

public class Notification implements Parcelable {
    private String title;
    private long currentTimeInMS;
    private String dateTimeString;
    private String type;
    private String attributesString;
    private String result;
    private Boolean isAccepted;
    private Boolean needAction;
    private String organization;

    public Notification(String title, String organization, long currentTimeInMS, String dateTimeString, String type, String attributesString,
            String result, Boolean isAccepted, Boolean needAction) {
        this.title = title;
        this.organization = organization;
        this.currentTimeInMS = currentTimeInMS;
        this.dateTimeString = dateTimeString;
        this.type = type;
        this.attributesString = attributesString;
        this.result = result;
        this.isAccepted = isAccepted;
        this.needAction = needAction;
    }

    protected Notification(Parcel in) {
        title = in.readString();
        currentTimeInMS = in.readLong();
        dateTimeString = in.readString();
        type = in.readString();
        attributesString = in.readString();
        result = in.readString();
        byte tmpIsAccepted = in.readByte();
        isAccepted = tmpIsAccepted == 0 ? null : tmpIsAccepted == 1;
        byte tmpNeedAction = in.readByte();
        needAction = tmpNeedAction == 0 ? null : tmpNeedAction == 1;
        organization = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeLong(currentTimeInMS);
        dest.writeString(dateTimeString);
        dest.writeString(type);
        dest.writeString(attributesString);
        dest.writeString(result);
        dest.writeByte((byte) (isAccepted == null ? 0 : isAccepted ? 1 : 2));
        dest.writeByte((byte) (needAction == null ? 0 : needAction ? 1 : 2));
        dest.writeString(organization);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Notification> CREATOR = new Creator<Notification>() {
        @Override
        public Notification createFromParcel(Parcel in) {
            return new Notification(in);
        }

        @Override
        public Notification[] newArray(int size) {
            return new Notification[size];
        }
    };

    @Override
    public String toString() {
        return "Notification{" +
                "title='" + title + '\'' +
                ", currentTimeInMS=" + currentTimeInMS +
                ", dateTimeString='" + dateTimeString + '\'' +
                ", type='" + type + '\'' +
                ", attributesString='" + attributesString + '\'' +
                ", result='" + result + '\'' +
                ", isAccepted=" + isAccepted +
                ", needAction=" + needAction +
                '}';
    }


    public JSONObject getJSONObject(){
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(attributesString);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getDateTimeString() {
        return dateTimeString;
    }

    public void setDateTimeString(String dateTimeString) {
        this.dateTimeString = dateTimeString;
    }

    public String getAttributesString() {
        return attributesString;
    }

    public void setAttributesString(String attributesString) {
        this.attributesString = attributesString;
    }

    public long getCurrentTimeInMS() {
        return currentTimeInMS;
    }

    public void setCurrentTimeInMS(long currentTimeInMS) {
        currentTimeInMS = currentTimeInMS;
    }

    public Boolean getAccepted() {
        return isAccepted;
    }

    public void setAccepted(Boolean accepted) {
        isAccepted = accepted;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Boolean getNeedAction() {
        return needAction;
    }

    public void setNeedAction(Boolean needAction) {
        this.needAction = needAction;
    }

}*/
