package com.example.guardianapp.ui.notifications;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.guardianapp.CredentialOffersViewModel;
import com.example.guardianapp.R;
import com.example.guardianapp.SingletonClass;
import com.example.guardianapp.Util;
import com.example.guardianapp.databinding.FragmentNotificationsBinding;
import com.example.guardianapp.db.Database;
import com.example.guardianapp.db.entity.Connection;
import com.example.guardianapp.db.entity.Credential;
import com.example.guardianapp.lib.Credentials;
import com.example.guardianapp.lib.Messages;
import com.example.guardianapp.lib.message.Message;
import com.example.guardianapp.lib.message.MessageType;
import com.example.guardianapp.model.Attribute;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class NotificationsFragment extends Fragment {
    private static String TAG = NotificationsFragment.class.getName();

    private NotificationAdapter mNotificationAdapter;
    private FragmentNotificationsBinding mNotificationsBinding;
    private Database db;
    private CredentialOffersViewModel model;

    public View onCreateView(@NonNull LayoutInflater inflater,
            ViewGroup container, Bundle savedInstanceState) {
        mNotificationsBinding = FragmentNotificationsBinding.inflate(inflater, container, false);
        return mNotificationsBinding.getRoot();
    }

    private void setNotificationAdapter(List<Credential> data) {
        if (mNotificationAdapter == null) {
            mNotificationAdapter = new NotificationAdapter(data, getContext());
            mNotificationsBinding.rvCredentialOffersListNotifications.setAdapter(
                    mNotificationAdapter);
        } else {
            mNotificationAdapter.setCredentials(data);
        }

        if (data.size() == 0) {
            mNotificationsBinding.tvNoNotificationMessage.setText(
                    getString(R.string.no_notifications));
            mNotificationsBinding.tvNoNotificationMessage.setVisibility(View.VISIBLE);
        } else {
            mNotificationsBinding.tvNoNotificationMessage.setVisibility(View.GONE);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mNotificationsBinding.rvCredentialOffersListNotifications.setHasFixedSize(false);
        mNotificationsBinding.rvCredentialOffersListNotifications.setLayoutManager(
                new LinearLayoutManager(getContext()));

        SingletonClass singleton = SingletonClass.getInstance(getContext());
        if (singleton.isInDemoMode()) {
            setNotificationAdapter(singleton.getCredentialList());
        } else {
            model = new ViewModelProvider(requireActivity()).get(CredentialOffersViewModel.class);
            model.getCredentialOffers().observe(getViewLifecycleOwner(),
                    data -> {
                        Log.d(TAG, "observe-data" + data.size());
                        setNotificationAdapter(data);
                    });
        }

        mNotificationsBinding.fabRefreshNotification.setOnClickListener(v -> {
            SingletonClass singletonClass = SingletonClass.getInstance(getContext());
            showLoadingCredentialsUI();
            if (singletonClass.isInDemoMode()) {
                if (singletonClass.isNoPendingCredentialOffers()) {
                    if (singletonClass.isGuardianConnected()) {
                        mNotificationsBinding.tvNoNotificationMessage.setVisibility(View.GONE);
                        singletonClass.refreshButtonClicked();
                        mNotificationsBinding.rvCredentialOffersListNotifications.scrollToPosition(
                                singletonClass.getCredentialList().size() - 1);
                    } else {
                        Toast.makeText(getContext(),
                                getString(R.string.guardian_not_connected_scan_qr_to_connect),
                                Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getContext(), getString(R.string.accept_pending_notifications),
                            Toast.LENGTH_SHORT).show();
                }
                loadedCredentialsUI();
            } else {
                model.getNewCredentialOffers().observeOnce(getViewLifecycleOwner(),
                        ok -> {
                            Log.d(TAG, "NewCredentials:" + ok);
                            loadedCredentialsUI();
                        });
                //checkCredentialOffers();
            }

        });


        mNotificationsBinding.rvCredentialOffersListNotifications.addOnScrollListener(
                new RecyclerView.OnScrollListener() {
                    @Override
                    public void onScrollStateChanged(@NonNull RecyclerView recyclerView,
                            int newState) {
                        if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                            mNotificationsBinding.fabRefreshNotification.show();
                        }
                        super.onScrollStateChanged(recyclerView, newState);
                    }

                    @Override
                    public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                        if (dy > 0 || dy < 0
                                && mNotificationsBinding.fabRefreshNotification.isShown()) {
                            mNotificationsBinding.fabRefreshNotification.hide();
                        }
                        super.onScrolled(recyclerView, dx, dy);
                    }
                });
    }

    private void showLoadingCredentialsUI() {
        mNotificationsBinding.pbLoading.setVisibility(View.VISIBLE);
        mNotificationsBinding.tvNoNotificationMessage.setText(
                R.string.loading_notifications);
        mNotificationsBinding.tvNoNotificationMessage.setVisibility(View.VISIBLE);
        mNotificationsBinding.fabRefreshNotification.setVisibility(View.GONE);
    }

    private void loadedCredentialsUI() {
        Log.d(TAG, "loadedCredentialsUI");
        getActivity().runOnUiThread(() -> {
            Log.d(TAG, "loadedCredentialsUI2");
            mNotificationsBinding.pbLoading.setVisibility(View.GONE);
            mNotificationsBinding.tvNoNotificationMessage.setVisibility(View.GONE);
            mNotificationsBinding.fabRefreshNotification.setVisibility(View.VISIBLE);
        });

    }

    private void loadCredentialOffers() {

        SingletonClass singletonClass = SingletonClass.getInstance(getContext());
        if (singletonClass.isInDemoMode()) {
            setNotificationAdapter(singletonClass.getCredentialList());
        } else {
            model.loadCredentialOffers();
            //TODO: if anything has to be during "onResume" in LIVE mode.
        }


    }

    private void checkCredentialOffers() {

        List<Connection> connections = db.connectionDao().getAll();
        List<Credential> creds = db.credentialsDao().getAll();
        for (Credential credential : creds) {
            Log.d(TAG, "CRED offersLOG:" + credential.toString() + "!");
        }
        for (Connection c : connections) {
            Util.printLogs(TAG, "connection", c.toString());
            Messages.getPendingMessages(c.serialized, MessageType.CREDENTIAL_OFFER).handle(
                    (res, throwable) -> {
                        if (throwable != null) {
                            throwable.printStackTrace();
                        }
                        if (res != null) {
                            for (Message message : res) {
                                CredDataHolder holder = extractDataFromCredentialsOfferMessage(
                                        message);
                                Log.d(TAG, "holder.id:" + holder.id);
                                Log.d(TAG, "holder.attributes:" + holder.attributes);
                                Log.d(TAG, "c.id:" + c.id);

                                if (!db.credentialsDao().checkOfferExists(holder.attributes,
                                        c.id)) {
                                    Credentials.createWithOffer(c.serialized,
                                            UUID.randomUUID().toString(), holder.offer).handle(
                                            (co, err) -> {
                                                if (err != null) {
                                                    err.printStackTrace();
                                                } else {
                                                    Credential offer = new Credential();

                                                    offer.claimId = holder.id;
                                                    offer.name = holder.name;
                                                    offer.organizationName = c.name;
                                                    offer.connectionId = c.id;
                                                    offer.attributes = holder.attributes;
                                                    offer.serialized = co;
                                                    offer.messageId = message.getUid();
                                                    offer.setNeedAction(true);
                                                    db.credentialsDao().insertAll(offer);
                                                }
                                                loadCredentialOffers();
                                                return null;
                                            });
                                } else {
                                    Log.d("CredentialOffersVM", "exists");
                                }
                            }
                        }
                        return res;
                    });
        }

    }

    private CredDataHolder extractDataFromCredentialsOfferMessage(Message msg) {
        try {
            JSONObject data = new JSONArray(msg.getPayload()).getJSONObject(0);
            String id = data.getString("claim_id");
            String name = data.getString("claim_name");
            JSONObject attributesJson = data.getJSONObject("credential_attrs");
            StringBuilder attributes = new StringBuilder();
            Iterator<String> keys = attributesJson.keys();
            ArrayList<Attribute> attributeArrayList = new ArrayList<>();
            while (keys.hasNext()) {
                String key = keys.next();
                String value = attributesJson.getString(key);
                Attribute attribute = new Attribute(key, value);
                attributeArrayList.add(attribute);
                attributes.append(String.format("%s: %s\n", key, value));
            }
            return new CredDataHolder(id, name, attributeArrayList, attributesJson.toString(),
                    msg.getPayload());
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    static class CredDataHolder {
        String id;
        String name;
        String attributes;
        ArrayList<Attribute> mAttributeArrayList;
        String offer;

        public CredDataHolder(String id, String name, ArrayList<Attribute> attributeArrayList,
                String attributes, String offer) {
            this.id = id;
            this.name = name;
            this.attributes = attributes;
            this.attributes = attributes;
            this.offer = offer;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "onStart");
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = Database.getInstance(getContext());
        Log.d(TAG, "onCreate");
    }

    @Override
    public void onResume() {
        super.onResume();
        loadCredentialOffers();
        Log.d(TAG, "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        //model.getCredentialOffers().removeObservers(getViewLifecycleOwner());
        Log.d(TAG, "onDestroyView");
    }
}