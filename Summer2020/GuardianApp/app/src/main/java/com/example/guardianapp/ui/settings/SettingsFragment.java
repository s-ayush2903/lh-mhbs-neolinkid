package com.example.guardianapp.ui.settings;

import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;

import android.app.AlertDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.guardianapp.R;
import com.example.guardianapp.SingletonClass;
import com.example.guardianapp.databinding.FragmentSettingsBinding;

public class SettingsFragment extends Fragment {

    private SettingsViewModel settingsViewModel;
    private static String TAG = SettingsFragment.class.getName();
    private FragmentSettingsBinding mSettingsBinding;

    public View onCreateView(@NonNull LayoutInflater inflater,
            ViewGroup container, Bundle savedInstanceState) {
        settingsViewModel = new ViewModelProvider(this).get(SettingsViewModel.class);
        mSettingsBinding = FragmentSettingsBinding.inflate(inflater, container, false);
        return mSettingsBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mSettingsBinding.switchModeSettings.setLabelOff(getString(R.string.demo));
        mSettingsBinding.switchModeSettings.setLabelOn(getString(R.string.live));

        hideDemoUI(SingletonClass.getInstance(getContext()).isInDemoMode());

        mSettingsBinding.clRestartDemoSettings.setOnClickListener(v -> showReset());

        mSettingsBinding.switchModeSettings.setOnToggledListener((toggleableView, isOn) -> {
            hideDemoUI(!isOn);
            Toast.makeText(getContext(),
                    getString(R.string.app_mode) + (isOn ? getString(R.string.live) : getString(
                            R.string.demo)),
                    Toast.LENGTH_SHORT).show();
            SingletonClass.getInstance(getContext()).setInDemoMode(!isOn);
        });


        mSettingsBinding.switchDeveloperSettings.setOnToggledListener((toggleableView, isOn) -> {
            Toast.makeText(getContext(),
                    getString(R.string.developer_mode) + (isOn ? getString(R.string.yes)
                            : getString(R.string.no)),
                    Toast.LENGTH_SHORT).show();
            SingletonClass.getInstance(getContext()).setDeveloperMode(isOn);
        });

    }

    void hideDemoUI(boolean isInDemoMode) {
        mSettingsBinding.switchModeSettings.setOn(!isInDemoMode);
        if (isInDemoMode) {
            mSettingsBinding.clRestartDemoSettings.setVisibility(View.VISIBLE);
        } else {
            mSettingsBinding.clRestartDemoSettings.setVisibility(View.GONE);
        }
        mSettingsBinding.clDeveloperSettings.setVisibility(View.GONE);
    }

    public void showReset() {
        AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(getContext());

        alertDialog2.setTitle(R.string.rest_demo_credentials); // Setting Dialog Title
        alertDialog2.setMessage(R.string.are_you_sure); // Setting Dialog Message
        alertDialog2.setIcon(R.drawable.ic_baseline_refresh_24); // Setting Icon to Dialog

        alertDialog2.setPositiveButton(R.string.yes, // Setting Positive "Yes" Btn
                (dialog, which) -> SingletonClass.getInstance(getContext()).restartDemo());

        alertDialog2.setNegativeButton(R.string.no, // Setting Negative "NO" Btn
                (dialog, which) -> dialog.cancel());

        alertDialog2.show(); // Showing Alert Dialog
    }
}