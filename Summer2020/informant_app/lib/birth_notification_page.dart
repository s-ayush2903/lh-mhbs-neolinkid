import 'package:flutter/material.dart';

import 'custom_app_bar.dart';
import 'routes/router.gr.dart';
import 'util/style.dart';

class BirthNotificationPage extends StatefulWidget {
  @override
  _BirthNotificationPageState createState() => _BirthNotificationPageState();
}

class _BirthNotificationPageState extends State<BirthNotificationPage> {
  TextEditingController childBirthDate = TextEditingController();
  TextEditingController issueDate = TextEditingController();
  String childSex;
  String typeOfBirth;
  String natureOfBirth;
  String subCounty;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: <Widget>[
              Text(
                "Birth Notification Credential",
                style: TextStyle(color: Colors.blue, fontSize: 28),
              ),
              Text(
                "Enter details below",
                style: TextStyle(color: Colors.blue, fontSize: 20),
              ),
              SizedBox(
                height: 20,
              ),
              /*listOfFields(widget.credentialType),*/
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Serial Number"),
                  SizedBox(
                    height: 4,
                  ),
                  TextFormField(
                    textInputAction: TextInputAction.next,
                    keyboardType: TextInputType.number,
                    decoration: textFormFieldDecoration,
                    autocorrect: false,
                  ),
                  SizedBox(
                    height: 16,
                  ),

                  //
                  Text("Child's First Name"),
                  SizedBox(
                    height: 4,
                  ),
                  TextFormField(
                    decoration: textFormFieldDecoration,
                    onTap: (){

                    },
                    autocorrect: false,
                  ),
                  SizedBox(
                    height: 16,
                  ),

                  //
                  Text("Child's Middle Name"),
                  SizedBox(
                    height: 4,
                  ),
                  TextFormField(
                    decoration: textFormFieldDecoration,
                    autocorrect: false,
                  ),
                  SizedBox(
                    height: 16,
                  ),

                  //
                  Text("Child's Fathers Name"),
                  SizedBox(
                    height: 4,
                  ),
                  TextFormField(
                    decoration: textFormFieldDecoration,
                    textInputAction: TextInputAction.next,
                    autocorrect: false,
                  ),
                  SizedBox(
                    height: 16,
                  ),

                  //
                  Text("Child's Birthdate"),
                  SizedBox(
                    height: 4,
                  ),
                  TextFormField(
                    controller: childBirthDate,
                    decoration: textFormFieldDecoration,
                    //initialValue: birthDate,
                    onTap: () async {
                      FocusScope.of(context).requestFocus(new FocusNode());
                      var date = await showDatePicker(
                          context: context,
                          initialDate:DateTime.now(),
                          firstDate:DateTime(1980),
                          lastDate:DateTime.now());
                      FocusScope.of(context).requestFocus(new FocusNode());
                      setState(() {
                        childBirthDate.text = "${date.day}/${date.month}/${date.year}";
                      });

                    },
                    autocorrect: false,
                  ),
                  SizedBox(
                    height: 16,
                  ),

                  //
                  Text("Child's Sex"),
                  SizedBox(
                    height: 4,
                  ),
                  DropdownButtonFormField(
                    onChanged: (newValue){
                      setState(() => childSex = newValue);
                    } ,
                    items: <String>['Male', 'Female','Unknown'].map((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                    decoration: textFormFieldDecoration,
                    value: childSex,
                  ),
                  SizedBox(
                    height: 16,
                  ),

                  //
                  Text("Type of Birth"),
                  SizedBox(
                    height: 4,
                  ),
                  DropdownButtonFormField(
                    onChanged: (newValue){
                      setState(() => typeOfBirth = newValue);
                    } ,
                    items: <String>['Single','Twin','Other'].map((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                    decoration: textFormFieldDecoration,
                    value: typeOfBirth,
                  ),
                  SizedBox(
                    height: 16,
                  ),

                  Text("Nature of Birth"),
                  SizedBox(
                    height: 4,
                  ),
                  DropdownButtonFormField(
                    onChanged: (newValue){
                      setState(() => natureOfBirth = newValue);
                    } ,
                    items: <String>['Alive', 'Dead'].map((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                    decoration: textFormFieldDecoration,
                    value: natureOfBirth,
                  ),
                  SizedBox(
                    height: 16,
                  ),

                  Text("Place of Birth"),
                  SizedBox(
                    height: 4,
                  ),
                  TextFormField(
                    decoration: textFormFieldDecoration,
                    textInputAction: TextInputAction.next,
                    autocorrect: false,
                  ),
                  SizedBox(
                    height: 16,
                  ),

                  Text("Sub-county"),
                  SizedBox(
                    height: 4,
                  ),
                  DropdownButtonFormField(
                    onChanged: (newValue){
                      setState(() => subCounty = newValue);
                    } ,
                    items: <String>['Turbo', 'Kessen', 'Moiben', 'Kapseret','Soy','Ainabkoi','Eldoret'].map((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                    decoration: textFormFieldDecoration,
                    value: subCounty,
                  ),
                  SizedBox(
                    height: 16,
                  ),

                  //
                  Text("Mother's First Name"),
                  SizedBox(
                    height: 4,
                  ),
                  TextFormField(
                    decoration: textFormFieldDecoration,
                    textInputAction: TextInputAction.next,
                    autocorrect: false,
                  ),
                  SizedBox(
                    height: 16,
                  ),

                  //
                  Text("Mother's Middle Name"),
                  SizedBox(
                    height: 4,
                  ),
                  TextFormField(
                    decoration: textFormFieldDecoration,
                    textInputAction: TextInputAction.next,
                    autocorrect: false,
                  ),
                  SizedBox(
                    height: 16,
                  ),

                  //
                  Text("Mother's Father's Name"),
                  SizedBox(
                    height: 4,
                  ),
                  TextFormField(
                    decoration: textFormFieldDecoration,
                    textInputAction: TextInputAction.next,
                    autocorrect: false,
                  ),
                  SizedBox(
                    height: 16,
                  ),

                  //
                  Text("Issued To (Name)"),
                  SizedBox(
                    height: 4,
                  ),
                  TextFormField(
                    decoration: textFormFieldDecoration,
                    textInputAction: TextInputAction.next,
                    autocorrect: false,
                  ),
                  SizedBox(
                    height: 16,
                  ),

                  Text("ID Number"),
                  SizedBox(
                    height: 4,
                  ),
                  TextFormField(
                    textInputAction: TextInputAction.next,
                    keyboardType: TextInputType.number,
                    decoration: textFormFieldDecoration,
                    autocorrect: false,
                  ),
                  SizedBox(
                    height: 16,
                  ),

                  //
                  /*Text("Issue Date"),
                  SizedBox(
                    height: 4,
                  ),
                  TextFormField(
                    controller: issueDate,
                    decoration: textFormFieldDecoration,
                    //initialValue: birthDate,
                    onTap: () async {
                      FocusScope.of(context).requestFocus(new FocusNode());
                      var date = await showDatePicker(
                          context: context,
                          initialDate:DateTime.now(),
                          firstDate:DateTime(1980),
                          lastDate:DateTime.now());
                      FocusScope.of(context).requestFocus(new FocusNode());
                      setState(() {
                        issueDate.text = "${date.day}/${date.month}/${date.year}";
                      });

                    },
                    autocorrect: false,
                  ),
                  SizedBox(
                    height: 16,
                  ),*/

                ],
              ),
              RaisedButton(
                color: Colors.blue,
                onPressed: () {
                  showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      // return object of type Dialog
                      return AlertDialog(
                        title: Text("Confirmation", style: TextStyle(color: Colors.black),),
                        content: Text("Are the following details correct?",style: TextStyle(color: Colors.black),),
                        actions: <Widget>[
                          // usually buttons at the bottom of the dialog
                          FlatButton(
                            child: Text("Confirm",style: TextStyle(color: Colors.blue),),
                            onPressed: () {
                              Navigator.pop(context);
                              Router.navigator.pushNamed(
                                Router.sendingCredentialPage,
                                arguments: SendingCredentialPageArguments(
                                  credentialName: "Birth Notification",
                                ),
                              );
                            },
                          ),
                          FlatButton(
                            child: Text("Edit",style: TextStyle(color: Colors.black),),
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          ),
                        ],
                      );
                    },
                  );
                },
                child: Text(
                  "Submit",
                  style: TextStyle(color: Colors.white),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              RaisedButton(
                color: Colors.white,
                onPressed: () {},
                child: Text(
                  "Cancel",
                  style: TextStyle(color: Colors.black),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
