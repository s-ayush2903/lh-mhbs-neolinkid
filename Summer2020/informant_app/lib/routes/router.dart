import 'package:auto_route/auto_route_annotations.dart';
import 'package:informant_app/connected_page.dart';
import 'package:informant_app/get_qr_page.dart';
import 'package:informant_app/guardian_credential_page.dart';
import 'package:informant_app/guardian_link_page.dart';
import 'package:informant_app/sending_credential_page.dart';
import 'package:informant_app/vaccine_page.dart';

import '../birth_notification_page.dart';
import '../initial_page.dart';

@MaterialAutoRouter()
class $Router {
  @initial
  InitialPage intialPage;
  GetQRPage getQRPage;
  ConnectedPage connectedPage;
  GuardianCredentialPage guardianCredentialPage;
  BirthNotificationPage birthNotificationPage;
  GuardianLinkPage guardianLinkPage;
  VaccinePage vaccinePage;
  SendingCredentialPage sendingCredentialPage;
}
