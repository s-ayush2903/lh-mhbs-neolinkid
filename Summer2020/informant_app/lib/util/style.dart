import 'package:flutter/material.dart';

const textFormFieldDecoration = const InputDecoration(
  focusedBorder: OutlineInputBorder(
    borderRadius: const BorderRadius.all(const Radius.circular(5.0)),
    borderSide: BorderSide(
      color: Colors.blue,
    ),
  ),
  enabledBorder: OutlineInputBorder(
    borderRadius: const BorderRadius.all(const Radius.circular(5.0)),
    borderSide: BorderSide(
      color: Colors.grey,
      width: 1,
    ),
  ),
);